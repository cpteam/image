<?php
namespace CPTeam\Image\Saver\Result;

class BasicResult implements ISaverResult
{
	private $realPath;
	private $basename;
	private $relativePath;
	
	/**
	 * @return mixed
	 */
	public function getRelativePath()
	{
		return $this->relativePath;
	}
	
	/**
	 * @param mixed $relativePath
	 */
	public function setRelativePath($relativePath)
	{
		$this->relativePath = $relativePath;
	}
	
	/**
	 * @return mixed
	 */
	public function getRealPath()
	{
		return $this->realPath;
	}
	
	/**
	 * @param mixed $realPath
	 */
	public function setRealPath($realPath)
	{
		$this->realPath = $realPath;
	}
	
	/**
	 * @return mixed
	 */
	public function getBasename()
	{
		return $this->basename;
	}
	
	/**
	 * @param mixed $basename
	 */
	public function setBasename($basename)
	{
		$this->basename = $basename;
	}
	
}
