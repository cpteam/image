<?php

namespace CPTeam\Image\Saver\Result;

interface ISaverResult
{
	public function getRealPath();
	
	public function getBasename();
	
	public function getRelativePath();
}
