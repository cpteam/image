<?php

namespace CPTeam\Image\Saver\Callback;

/**
 * Class SaveThumbsCallback
 *
 * @package CPTeam\Image\Saver\Callback
 */
class SaveThumbsCallback implements ISaverCallback
{
	/**
	 * @var array
	 */
	protected $config = [
		'thumbs' => [
			'quality' => 80,
			'enabled' => true,
			'filter' => \Imagick::FILTER_CATROM,
			'blur' => 1,
			'bestfit' => true,
			'items' => [
				[
					'width' => 2048,
					'height' => 1536,
					'suffix' => '.preview',
					//inherit blur
					//inherit bestfit
				],
				[
					'width' => 200,
					'height' => 200,
					'suffix' => '.200x200',
					//inherit blur
					//'bestfit' => false //resize at any cost, image will be distorted
				],
			],
		],
	];
	
	/**
	 * @param $realPath
	 *
	 * @return void
	 */
	public function execute($realPath)
	{
		$imagick = new \Imagick($realPath);
		$fileName = pathinfo($realPath, PATHINFO_FILENAME);
		$dirName = pathinfo($realPath, PATHINFO_DIRNAME);
		$extension = pathinfo($realPath, PATHINFO_EXTENSION);
		if ($extension !== '') {
			$extension = '.' . $extension;
		}
		
		if ($this->config['thumbs']['enabled'] === true) {
			foreach ($this->config['thumbs']['items'] as $thumb) {
				/** @var \Imagick $newImage */
				$newImage = clone $imagick;
				
				$blur = isset($thumb['blur']) ? $thumb['blur'] : $this->config['thumbs']['blur'];
				$filter = isset($thumb['filter']) ? $thumb['filter'] : $this->config['thumbs']['filter'];
				$bestFit = isset($thumb['bestfit']) ? $thumb['bestfit'] : $this->config['thumbs']['bestfit'];
				$quality = isset($thumb['quality']) ? $thumb['quality'] : $this->config['thumbs']['quality'];
				
				$newImage->setImageCompressionQuality($quality);
				$newImage->resizeImage($thumb['width'], $thumb['height'], $filter, $blur, $bestFit);
				$newImage->writeImage($dirName . DIRECTORY_SEPARATOR . $fileName . $thumb['suffix'] . $extension);
			}
		}
	}
}
