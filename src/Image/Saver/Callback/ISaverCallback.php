<?php

namespace CPTeam\Image\Saver\Callback;

/**
 * Interface ISaverCallback
 *
 * @package CPTeam\Image\Saver\Callback
 */
interface ISaverCallback
{
	/**
	 * @param $realPath
	 *
	 * @return mixed
	 */
	public function execute($realPath);
}
