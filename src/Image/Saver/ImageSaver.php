<?php

namespace CPTeam\Image\Saver;

use CPTeam\Image\Saver\Callback\ISaverCallback;
use CPTeam\Image\Saver\Result\BasicResult;
use CPTeam\Image\Saver\Result\ISaverResult;
use Nette\Http\FileUpload;
use Nette\Utils\Random;

/**
 * Class ImageSaver
 *
 * @package CPTeam\Image\Saver
 */
class ImageSaver
{
	/**
	 * @var array
	 */
	private $config;
	
	/** @var array Callbacks executed after saving original image */
	private $afterSave = [];
	
	/**
	 * ImageSaver constructor.
	 *
	 * @param $config
	 */
	public function __construct($config)
	{
		$this->config = $config;
	}
	
	/**
	 * @param FileUpload $input
	 * @param string $nextDirsPath This will be appended after rootDir. Format is 'mydir/' or 'mydir/subdir/'
	 * @param string $forcedPath Overwrite automatic path generation
	 *
	 * @return ISaverResult
	 */
	public function save(FileUpload $input, $nextDirsPath = '', $forcedPath = '', $callbacksDisabled = false)
	{
		if ($input->isOk() && $input->isImage()) {
			if ($forcedPath === '') {
				$avp = $this->getAvailablePath();
				$basename = $avp['token'] . '_' . $this->config['depth'];
				$absoluteFilePath = $avp['dir'] . $nextDirsPath . $basename;
			} else {
				$absoluteFilePath = $forcedPath;
				$basename = pathinfo($forcedPath, PATHINFO_BASENAME);
			}
			
			$input->move($absoluteFilePath);
			
			if (!$callbacksDisabled) {
				//execute afterSave callbacks
				/** @var ISaverCallback $saverCallback */
				foreach ($this->afterSave as $saverCallback) {
					$saverCallback->execute($absoluteFilePath);
				}
			}
			
			$result = new BasicResult();
			$result->setRealPath($absoluteFilePath);
			$result->setRelativePath(substr($absoluteFilePath, strlen($this->config['wwwDir'])));
			$result->setBasename($basename);
			
			return $result;
		} else {
			throw new \InvalidArgumentException('Input is not and image');
		}
	}
	
	public function saveToDir($dir, FileUpload $input, $options = [])
	{
		$avp = $this->getAvailablePath($dir);
		$basename = $avp['token'] . '_' . $this->config['depth'];
		$absoluteFilePath = $avp['dir'] . $basename;
		
		return $this->save($input, '', $absoluteFilePath);
	}
	
	//public function save()
	
	/**
	 * @param ISaverCallback $callback
	 */
	public function addAfterSaveCallback(ISaverCallback $callback)
	{
		$this->afterSave[] = $callback;
	}
	
	/**
	 * @param string $name
	 * @param string $nextDirsPath Tells which directories should be appended to the path
	 *
	 * @return string
	 */
	/*
	public function getPathFromName($name, $nextDirsPath = '')
	{
		if(is_string($name) && strlen($name) >= $this->config['depth']) {
			$dirs = '';
			for($i = 0; $i < $this->config['depth']; $i++) {
				$dirs .= $name[$i] . DIRECTORY_SEPARATOR;
			}
			$path = $this->config['rootDir'] . $nextDirsPath  . $dirs . $name;
			return $path;
		} else {
			throw new \InvalidArgumentException('Name must be a string and length must be greater or equal than 3');
		}
	}
	*/
	
	public static function getSrc($rootDir, $name, $nextDirsPath = '')
	{
		$sp = strpos($name, '_');
		if ($sp !== false || isset($name[$sp + 1])) {
			$depth = $name[$sp + 1];
			
			$dirs = '';
			for ($i = 0; $i < $depth; $i++) {
				$dirs .= $name[$i] . DIRECTORY_SEPARATOR;
			}
			
			$path = $rootDir . $nextDirsPath . $dirs . $name;
			
			return $path;
		} else {
			throw new \LogicException('Depth separator "_" not found in filename');
		}
	}
	
	public static function getSrcRelative($rootDir, $wwwDir, $name, $nextDirsPath = '')
	{
		return substr(self::getSrc($rootDir, $name, $nextDirsPath), strlen($wwwDir));
	}
	
	public function getConfig()
	{
		return $this->config;
	}
	
	/**
	 * @return string
	 */
	private function getToken()
	{
		return Random::generate(
			$this->config['token']['length'],
			'0-9a-zA-Z'
		);
	}
	
	/**
	 * @param array $tokenDirectory
	 *
	 * @return string
	 */
	private function getDir($tokenDirectory, $rootDir = null)
	{
		$path = '';
		foreach ($tokenDirectory as $item) {
			$path .= $item . DIRECTORY_SEPARATOR;
		}
		$dir = ($rootDir ? $rootDir : $this->config['rootDir']) . $path;
		
		return $dir;
	}
	
	/**
	 * @param string $token
	 *
	 * @return array
	 */
	private function buildDirs($token)
	{
		if (isset($token[$this->config['token']['length'] - 1]) == false) {
			throw new \LogicException('Length of token must be ' . $this->config['token']['length'] . ' characters');
		}
		
		$tokens = [];
		for ($i = 0; $i < $this->config['depth']; $i++) {
			$tokens[] = $token[$i];
		}
		
		return $tokens;
	}
	
	/**
	 * @return array
	 */
	private function getAvailablePath($rootDir = null)
	{
		do {
			$token = $this->getToken();
			$tokenDirectory = $this->buildDirs($token);
			$dir = $this->getDir($tokenDirectory, $rootDir);
		} while (
		file_exists($dir . $token)
		);
		
		return [
			'dir' => $dir,
			'dirParts' => $tokenDirectory,
			'token' => $token,
		];
	}
	
}
