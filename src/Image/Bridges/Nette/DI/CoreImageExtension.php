<?php
namespace CPTeam\Image\Bridges\Nette\DI;

use CPTeam\Image\Saver\Callback\ISaverCallback;
use CPTeam\Image\Saver\ImageSaver;
use Nette\DI\CompilerExtension;

/**
 * Class CoreImageExtension
 *
 * @package CPTeam\Image\Bridges\NetteDI
 */
class CoreImageExtension extends CompilerExtension
{
	/**
	 * @var array
	 */
	public $defaults = [
		'depth' => 3,
		'token' => [
			'length' => 8,
		],
		'callbacks' => [
			'enabled' => true,
			'items' => [],
		],
	];
	
	/**
	 * @inheritdoc
	 */
	public function loadConfiguration()
	{
		$config = $this->getConfig($this->defaults);
		if (!isset($config['rootDir'])) {
			throw new \InvalidArgumentException('rootDir is not defined.');
		}
		if ($config['depth'] > $config['token']['length']) {
			throw new \InvalidArgumentException('depth cannot be bigger than token length');
		}
		
		$builder = $this->getContainerBuilder();
		
		if (!isset($builder->parameters['wwwDir'])) {
			throw new \LogicException('wwwDir is not set in parameters');
		}
		
		$config['wwwDir'] = $builder->parameters['wwwDir'];
		
		$serviceDefinition = $builder->addDefinition($this->prefix('imageSaver'))
			->setClass(ImageSaver::class)
			->setArguments([$config]);
		
		if ($config['callbacks']['enabled'] === true) {
			foreach (array_unique($config['callbacks']['items']) as $callback) {
				/** @var ISaverCallback $callbackClass */
				$serviceDefinition->addSetup('addAfterSaveCallback', [$callback]);
			}
		}
	}
}
