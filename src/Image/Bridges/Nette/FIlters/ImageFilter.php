<?php
namespace CPTeam\Image\Bridges\Nette\Filters;

use CPTeam\Image\Saver\ImageSaver;
use Kocky\Model\Entity\UserImage;
use Nette\Object;

class ImageFilter extends Object
{
	public $rootDir;
	
	public function __invoke($filter, $value)
	{
		$args = func_get_args();
		if (method_exists(__CLASS__, $filter)) {
			array_shift($args);
			
			return call_user_func_array([__CLASS__, $filter], $args);
		} else {
			return call_user_func_array(function () {
				/*if(func_num_args() < 3) {
					throw new \InvalidArgumentException('Unknown filter or not enough parameters passed');
				}*/
				$args = func_get_args();
				$filter = substr($args[0], 1);
				
				$image = $args[1];
				if ($image instanceof UserImage) {
					/** @var UserImage $image */
					$hash = $image->hash;
				} else {
					$hash = $image;
				}
				$glue = isset($args[3]) ?: '.';
				
				//todo check if image exist
				//todo if image does not exist, create it
				if ($filter == 'orig') {
					return ImageSaver::getSrc($this->rootDir, $hash);
				}
				
				return ImageSaver::getSrc($this->rootDir, $hash . $glue . $filter);
			}, $args);
		}
	}
	
	/**
	 * @param mixed $rootDir
	 */
	public function setRootDir($rootDir)
	{
		$this->rootDir = $rootDir;
	}
	
}
